function makePerson(name){
	return {
		name:name,
		sayHello: function(){ return 'Hi, I am ' + name }
	}
}
alice = makePerson('Alice')
alice.sayHello()

bob = makePerson('Bob')
bob.sayHello()

alice.sayHello == bob.sayHello
//////////////////////////////////////////
function sayHello(){ return 'Hi, I am ' + this.name }

function makePerson(name){
	return {
		name:name,
		sayHello: sayHello
	}
}
alice = makePerson('Alice')
alice.sayHello()
// "Hi, I am Alice"
sayHello()
// "Hi, I am "
name = "Window"
// "Window"
sayHello()
// "Hi, I am Window"
window.sayHello()
// "Hi, I am Window"

bob = makePerson('Bob')
// {name: "Bob", sayHello: ƒ}
alice.sayHello == bob.sayHello
// true
//////////////////////////////////////////
function Person(name){
    console.log(this) 
}
Person('Alice')
// Window
new Person('Alice')
// Person{}
///////////////////////////////////
function Person(name){
    this.name = name 
}
Person.prototype.sayHello = function(){ return 'Hi, I am ' + this.name }

alice = new Person('Alice')
// Person {name: "Alice"}
alice.sayHello()
// "Hi, I am Alice"
alice
// Person {name: "Alice"}
bob = new Person('Bob')
// Person {name: "Bob"}
alice.sayHello == bob.sayHello
// true
////////////////////////////////
div = document.createElement('div')
div.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__
///////////////////////////////


function Person(name){
    this.name = name 
}
Person.prototype.sayHello = function(){ return 'Hi, I am ' + this.name }

alice = new Person('Alice')

function Employee(name,salary){
	Person.apply(this,arguments)
// 	PayrollSubject.apply(this,arguments)
	
	this.salary = salary
}
Employee.prototype = Object.create(Person.prototype)
Employee.prototype.work = function(){ return 'I want my '+this.salary }

tom = new Employee('Tom',1200)
// Employee {name: "Tom", salary: 1200}
tom.sayHello()
// "Hi, I am Tom"
tom.work()
// "I want my 1200"
tom
// Employee {name: "Tom", salary: 1200}
tom instanceof Employee
// true
tom instanceof Person
// true

//////////////////////////////////////////////
// EcmaSCript 2015 +  
// Starsze przegladarki - https://babeljs.io/

class Person{
    constructor(name){ this.name = name }
    sayHello(){ return 'Hi,'+this.name }
}
// undefined
alice = new Person('Alice')
// Person {name: "Alice"}
class Employee extends Person{
    constructor(name,salary){
        super(name)
        this.salary = salary
    }
    work(){ return this.salary}
}
// undefined
tom = new Employee('Tom',1200)

//////////////////////////////