
/* 
    DOMKNIĘCIA - CLOSURES:
*/

function test(){
	var zmienna = 'test'

	console.log(nie_ma_takiej())

	function nie_ma_takiej(){
		return ' zmienna : ' + zmienna
    }
}

test()

////////////////////////////

function dodawacz(ile){
	return function(ile_jeszcze){
		return ile + ile_jeszcze
    }
}

dodaj5 = dodawacz(5)

dodaj5(3) == 8

/////////////////////

function API(url, token, defaults){


	return {
		getPosts: function(ile){ return url+'/posts'+'?=token='+token},
		getPost: function(id){ return url+'/post'+id+'/?=token='+token }
    }
}

var api = API.getPost(123)


/// Imediately Invoked Function Formula / Module pattern - wzorzec izolujacy zmienne
x = ( function(a,b){
	/// lokalne zmienne

	return a+b;
} )(1,2)

//////////////////////////

var lista = [1,2,3,5,6]
undefined
var lista2 = []
for(var i = 0; i<lista.length; i++){
	lista2[i] = lista[i] * 3
}
18
lista2
(5) [3, 6, 9, 15, 18]
lista2 = lista.map(function(x){ return x * 3})
(5) [3, 6, 9, 15, 18]

///////////////////////////


isOdd = function(x){ return x % 2 != 0 }
multiply3 = function(x){ return x * 3}

lista
	.map(multiply3)
	.filter(isOdd)
	.reduce( function(sum, next) {
		sum += next;
		return sum;
    }, 0)
    
/////////////////////////

function reduce(array, reducer, initial){
	
	for(var i =0; i<array.length; i++){
		initial = reducer(initial,array[i])
    }
	return initial
}
reduce([1,2,3,4,5],function(sum,next){return sum+=next},0)

///////////////////////////

var points = [
    {team:'blue', score:12},
    {team:'red', score:9},
    {team:'blue', score:4},
    {team:'red', score:15},
    {team:'pink', score:43}
]

scores = points.reduce(function(teamScores, match){
	teamScores[match.team] = teamScores[match.team] || 0

	teamScores[match.team] += match.score

	return teamScores
},{
	//blue: 0, red: 0
})

////////



