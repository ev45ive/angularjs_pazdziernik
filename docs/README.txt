https://bitbucket.org/ev45ive/angularjs_pazdziernik

git clone https://bitbucket.org/ev45ive/angularjs_pazdziernik.git moj_katalog
cd moj_katalog
npm install
npm start


// Scopes

angular.bootstrap(document.body, ['my.module'])

angular.element($0).scope()

angular.element($0).scope().$root

angular.element($0).injector().get('$rootScope')

angular.element($0).injector().get('TasksService')

angular.element($0).scope().$digest()

angular.element($0).scope().$apply(function(){
    // wasz kod zmieniajacy scope
})

// TYLKO i WYŁĄCZNIE w Dyrektywach - w funkcji Link()
$scope.$watch('tasks', function(tasks){
    $scope.count = tasks.length
})