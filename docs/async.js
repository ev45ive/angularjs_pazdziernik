// https://rosettacode.org/wiki/Sorting_algorithms/Bogosort#JavaScript

// planujemy operacje "na zaraz"
setTimeout(function(){

	console.log('bardzo pilna funkcja! ')
},16)
// blokujemy glowny wątek:
bogosort([234,43,532,475,32554,44,5324,7,68,5,65])

/////////////////

function echo(text, callback){
	setTimeout(function(){
		callback(text)
    },2000)
}

echo('HaallooO!?', function(response) {
	console.log(response)
})

////////////////////////////
// Dane:
var promiseOfData = jQuery.getJSON('dane.json')

////
// Widoki:
promiseOfData.then(function(data){ console.log('Widok 1 ',data) })
promiseOfData.then(function(data){ console.log('Widok 2 ',data) })

//////////////////////////////
// https://github.com/taylorhakes/promise-polyfill
var promise = new Promise(function(resolve, reject){
	
	setTimeout(function(){
		resolve('dane')
    },2000)

})
///
promise.then( function(dane){
	console.log(dane)
})
///////////////////////////

function echo(text,err){
	return new Promise(function(resolve, reject){
        setTimeout(function(){
            err? reject(err) : resolve('> ' + text)
        },2000)
    })
}
///
var promise = echo('Alice','500 Server Error')

promise.then( function(dane){
	console.log(dane)
},function onErr(err){
	console.log('Blad ' + err)
})

//////////////////////////////////////////
function echo(text,err){
	return new Promise(function(resolve, reject){
        setTimeout(function(){
            err? reject(err) : resolve('> ' + text)
        },2000)
    })
}
p = echo('user 1', 'errr...')

p.then(function(user){
	return echo(user + ' posts ')
},function(err){
    return 'Wystaplil blad'
})

////// 

.then(function(posts){
    console.log(posts)
})
//////////////////////////////////////////