var gulp = require('gulp');
var concat = require('gulp-concat');
var inject = require('gulp-inject');
var es = require('event-stream')
var browserSync = require('browser-sync')
var angularFilesort = require('gulp-angular-filesort')
var rename = require('gulp-rename')

var server = browserSync.create()

var jsFiles = [
    'src/vendors/angular.min.js',
    'src/vendors/angular-ui-router.js',
    'src/vendors/**/*.js',   
    'src/**/*.module.js',  
    'src/**/*.js'
]
var cssFiles = [
    'src/vendors/bootstrap.css'
]
var watchFiles = jsFiles
.concat(cssFiles)
.concat([
    'src/**/*.tpl.html',
    'src/index.html'
]);

gulp.task('default',function(){
    console.log('Hello from default')
})

gulp.task('serve',['watch'],function(){
    server.init({
        server:{
            baseDir:'src'
        },
        port:8080
    })
    // gulp.watch(watchFiles, ['inject'])
    // .on('change',function(){
    //         server.reload()
    // })
})

// gulp.task('concat',function(){
//     gulp.src(jsFiles)
//         .pipe(concat('scripts.js'))
//         .pipe(gulp.dest('dist'))
// })

gulp.task('api',function(){
    // server.js
    const jsonServer = require('json-server')
    const server = jsonServer.create()
    const router = jsonServer.router('dane.json')
    const middlewares = jsonServer.defaults()

    server.use(middlewares)
    server.use(router)
    server.listen(3000, () => {
    console.log('JSON Server is running')
    })
})

gulp.task('templates', function(){
    gulp.src([
        'src/**/*.tpl.html',
        '!src/index.tpl.html'
    ])
    .pipe(gulp.dest('dist'))
})

gulp.task('watch',['npm-files'],function(){
    gulp.watch(watchFiles,['inject'])
    gulp.watch('src/**/*.tpl.html',function(){
        server.reload()
    })
})

gulp.task('inject',function(){
    
    var scriptsStream = gulp.src(jsFiles)
        // .pipe(angularFilesort())
    var stylesStream = gulp.src(cssFiles)
        
    gulp.src('./src/index.tpl.html')
    .pipe(inject(es.merge(
        scriptsStream, 
        stylesStream
    ),{
        relative:true
    }))
    .pipe(rename('index.html'))
    .pipe(gulp.dest('src'))
    .pipe(server.stream())
})

gulp.task('npm-files',function(){
    gulp.src([
        'node_modules/angular/angular.min.js',
        'node_modules/@uirouter/angularjs/release/angular-ui-router.js', 
        'node_modules/bootstrap/dist/css/bootstrap.css'
    ]).pipe(gulp.dest('src/vendors'))
})


gulp.task('build', ['templates'], function(){
    var scriptsStream = gulp.src(jsFiles)
        // .pipe(concat('scripts.js'))
        .pipe(gulp.dest('dist'))

    var stylesStream = gulp.src([
        'node_modules/bootstrap/dist/css/bootstrap.css'
    ])
    .pipe(gulp.dest('dist'))
        
    gulp.src('src/index.tpl.html')
    .pipe(inject(es.merge(
        scriptsStream, 
        stylesStream
    ),{
        relative:true
    }))
    .pipe(gulp.dest('dist'))
    .pipe(server.stream())
})


gulp.task('copy',function(){
    gulp.src('src/**/*.js')
        .pipe(gulp.dest('dist'))
})

