angular.module('app-module')

.directive('geoMap',function(){

    return {
        template:'<div ng-click="update()">\
        <img ng-src="https://maps.googleapis.com/maps/api/staticmap?center={{lat}},{{lng}}&size=300x300&zoom={{zoom}}">\
        </div>',
        scope:{
            lat:'=',
            lng:'=',
            zoom:'=',
            doUpdate:'&onUpdate'
        },
        controller: function($scope){
            $scope.update = function(){
                navigator.geolocation.getCurrentPosition(function(data){
                  $scope.$apply(function(){
                      $scope.lat = data.coords.latitude
                      $scope.lng = data.coords.longitude
                  })
                  $scope.doUpdate({lng:$scope.lng, lat:$scope.lat, timestamp: data.timestamp})
                })
            }
        }
    }
})

// Dyrektywa <user-card></user-card>
.directive('userCard',function(){

    return {
        // restrict: 'EACM',
        restrict: 'E',
        // templateUrl:''
        template:'<div>\
             <h6>{{title}} </h6>\
             User details: <b>{{user.name}}</b> \
             <button ng-click="onSelect({\
                $event:user\
             })">Select</button>\
        <div>',
        scope:{
            user:'=',
            title:'@',
            onSelect:'&onSelect'
        },
        link: function(scope, $elem, attrs){
            console.log('hello!',scope,$elem, attrs)

            $elem.find('h6').on('click',function(){
                scope.$apply(function(){

                    scope.title = "Clicked!"

                })
                // scope.$digest()
            })
        }
    }
})

.controller('directivesCtrl', function($scope){
    $scope.user = {
        name: 'Nowy User'
    }
    $scope.friend = {
        name: 'User`s Friend'
    }
    $scope.choose = function(selected){
        $scope.selected = selected
    }
})