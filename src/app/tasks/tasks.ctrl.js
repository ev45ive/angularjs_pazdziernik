angular.module('app-module')
.controller('TasksCtrl',function($scope, TasksService){
    console.log(TasksService)

    $scope.taskName = ''

    $scope.results = TasksService.results;

    // debugger

    $scope.refresh = function(){
        
        TasksService.fetch()
        .then(function(data){
            $scope.tasks = data;
        })

    }

    // $scope.completed = TasksService.completed
    $scope.completed = function(){
        return TasksService.completed()
    };

    $scope.remove = function(task){
        TasksService.remove(task)
    }

    $scope.addTask = function(){
        TasksService.addTask($scope.taskName)
        $scope.taskName = ''
    }
})