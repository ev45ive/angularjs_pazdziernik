angular.module('app-module')
.service('TasksService', function($http){

    this.results = {
        tasks:[]
    }

    var url = 'http://localhost:3000/todos/'

    var that = this;

    this.fetch = function(){
        return $http.get(url)
        .then(function(response){
            var data = response.data

            return that.results.tasks = data
        })      
    }

    this.addTask = function(taskTitle){
        this.results.tasks.push({
            title: taskTitle,
            completed: false
        })
    }

    this.remove = function(task){
        var index = this.results.tasks.indexOf(task)
        this.results.tasks.splice(index,1)
    }

    this.completed = function(){
        return this.results.tasks
        .reduce(function(count,task){
            count += task.completed? 1 : 0;
            return count;
        },0)
    }

})