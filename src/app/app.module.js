// get exisiting module
// var appmodule = angular.module('app-module')

// initialize new module
var myApp = angular.module('app-module',['ui.router'])


myApp.config(function($stateProvider) {

    $stateProvider.state('home',{
        url:'/', redirect:'/users'
    })
    $stateProvider.state('users',{
        url:'/users',
        redirect:'users.list',
        views:{
            '':{
                templateUrl:'app/users/users.tpl.html',
                controller:'UsersController as users'
            }
        }
    })
    $stateProvider.state('users.list',{
        url:'/',
        views:{
            list:{
                template: '\
                    <users-list\
                    count="count"\
                        users="users.list"\
                        selected="users.selected"\
                        on-remove="users.remove($event)"\
                        on-select="users.changeUser($event)">\
                    </users-list>\
                    <button ng-click="users.newUser()" class="btn btn-info">New User</button>\
                    <button ng-click="users.update()" class="btn btn-info">Refresg</button>'
            },
            details:{
                template:'Please select user'
            }
        }
    })
    $stateProvider.state('users.details',{
        url:'/:id',
        resolve: {
            'user': function(UsersService, $stateParams){
                return UsersService.get($stateParams.id)
            },
            'goTo':function($state){
                return function goTo(state,params){
                    console.log(state,params)
                    $state.go(state,params)
                }
            }
        },
        views:{
            list:{
                template: '\
                    <users-list\
                    count="count"\
                        users="users.list"\
                        selected="$resolve.user"\
                        on-remove="users.remove($event)"\
        on-select="$resolve.goTo( \'users.details\', $event)">\
                    </users-list>\
                    <button ng-click="users.newUser()" class="btn btn-info">New User</button>\
                    <button ng-click="users.update()" class="btn btn-info">Refresg</button>'
            },
            details:{
                template: '<h3> User Form</h3>\
                <user-form ng-if="$resolve.user"\
                    user="$resolve.user"\
                    on-save="users.saveUser(user)"\
                    on-cancel="users.selected = null"\
                ></user-form>'
            }
        }
    })

    .state('tasks',{
        url:'/todos', templateUrl:'app/tasks/tasks.tpl.html'
    })
    .state('people',{
        url:'/people', templateUrl:'app/people/people.tpl.html'
    })
    .state('directives',{
        url:'/directives', templateUrl:'app/directives/directives.tpl.html'
    })

});


myApp.value('api_url','http://localhost:3000/')
myApp.value('tasks_url','http://localhost:3000/todos/')
myApp.value('users_url','http://localhost:3000/users/')


/* 
    jQuery.getJSON('config.json').then(function(config){
        myApp.value('config',config)

        angular.bootstrap(document.body, ['app-module'])
    })

*/