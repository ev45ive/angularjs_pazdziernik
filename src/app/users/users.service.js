angular.module('app-module')
.service('UsersService',function($http, users_url, $q, $rootScope){

    var events = {
        users: 'users.update'
    }

    this.results = {
        users: []
    }

    this.get = function(id){
        // search for user in this.results.users, OR:
        return $http.get(users_url + id)
        .then(function(response){
            return response.data;
        })
    }

    this.fetch = function(){
        return $http
        .get(users_url)
        .then(function(users){
            this.results.users = users.data;
            $rootScope.$emit(events.users,users.data)
      
            return users.data
        }.bind(this))
    }
    
    this.fetch()

    this.subscribe = function(subscription){
        subscription(this.results.users)
        return $rootScope.$on(events.users,function(e,users){
            subscription(users)
        })
    }
    
    this.remove = function(user){
        return $http.delete(users_url+user.id)
        .then(function(){
            this.fetch()
            return {
                message: 'Removed!'
            }
        }.bind(this))
    }

    this.save = function(user){
        return $q(function(resolve) {
            if(user.id){
                // update
                resolve($http
                .put(users_url + user.id,user))
            }else{
                // create
                resolve($http
                .post(users_url,user))
            }
        })
        .then(function(response){
            this.fetch()
            return {
                data: response.data,
                message: 'Success!'
            }
        }.bind(this))
        .catch(function(err){
            return {
                data: user,
                message: 'Error!'
            }
        })
    }
})