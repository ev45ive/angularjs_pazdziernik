angular.module('app-module')
.controller('UsersController',function(UsersService){

    this.list = [
    ]

    this.$onDestroy = function(){
        this.unsubscribe()
    }

    this.remove = function(user){
        UsersService.remove(user)
        .then(function(resp){
            this.message = resp.message
            this.changeUser(null)
        }.bind(this))
    }

    this.unsubscribe = UsersService.subscribe(function(users){
        // this.message = "update!"
        this.list = users
    }.bind(this))

    this.update = function(){    
        UsersService.fetch()
    }

    this.message = ''
    
    this.selected = this.list[0]

    this.changeUser = function(user){
        this.selected = user
    }

    this.newUser = function(){
        this.selected = {}
    }

    this.saveUser = function(user){
        UsersService.save(user)
        .then(function(response){ 
            this.message = response.message;
            // this.changeUser(response.data)
            this.newUser()
        }.bind(this)) 
    }
})

.component('usersList',{
    // View
    templateUrl:'app/users/users-list.tpl.html',
    // ViewModel bindings to controller`s `this`
    bindings:{
        users:'<',
        selected:'<',
        // Evaluate code from on-select=""
        emitSelected:'&onSelect',
        onRemove:'&'
    },  
    // Controller  
    controller: function(/* $rootScope,$http */
        $scope
    ){
        this.selected;
        
        this.remove = function(user){
            this.onRemove({$event:user})
        }

        this.select = function(user){
            // Send selected user to parent
            this.emitSelected({
                $event:user
            })
        }
    },
    // controllerAs: '$ctrl'
})

/* 
// Angular 2 Components: 
@Component({
    selector:'users-list',
    inputs:{ users:'users'},
    // controller:Ctrl
})
class Ctrl{
    select(){
        this.selected
    }
} */

.directive('usersList_old',function(){

    return {
        // restrict:'EA',
        // template: '<div></div>'
        templateUrl:'app/users/users-list.tpl.html',
        scope:{
            users:'='
        },
        bindToController:true,
        // Only when direct manipulating DOM element:
        // link: function(scope, $elem, attrs, $ctrl){},
        // controller: 'UsersListController',
        // controller: 'UsersListController as $ctrl',
        controller: function($http){
            //this.users <-  <users-list users="x"
            this.selected
            this.select = function(user){
                this.selected = user
            }
        },
        controllerAs: '$ctrl'
    }
})


// Testing List Controller
/* function ListSelector(){
    this.selected;
    
    this.select = function(user){
        this.selected = user

        // Send selected user to parent
        
        this.emitSelected({
            '$event':user
        })
    }
}
$ctrl = new ListSelector()
// input.onclick = ?
$ctrl.emitSelected = function(context){ console.log('lisrt selected',+context.$event) }
$ctrl.select({id:1,name:'testuser'}) */