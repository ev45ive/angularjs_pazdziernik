angular.module('app-module')
/* 
<user-form 
    user="selected"
    on-save=""
    on-cancel=""
><user-form> 
*/
.component('userForm',{
    templateUrl:'app/users/user-form.tpl.html',
    bindings:{
        originalUser: '<user',
        onSave:'&',
        onCancel:'&'
    },
    controller: function(
        /* $scope, $element, $http, ... */
    ){
        // bindings are ready from here:
        this.$onInit = function(){
            // console.log(this.user)
        }

        // only for one-way bindings ( < )
        this.$onChanges = function(changesObj){
            // console.log(changesObj)
            // this.user = Object.assign({},this.originalUser)
            if(this.form && this.user){
                this.form.$setPristine()
                this.form.$setUntouched()
            }

            this.user = angular.copy(this.originalUser)
        }

        this.$doCheck = function(){
            // console.log('check')
        }

        this.$onDestroy = function(){
            // console.log('bye bye!')
        }

        this.$postLink = function(){
            // tutaj (nie polecam) modyfikujemy $element
        }
        
        this.save = function(){
            console.log(this.form)
            
            // this.onSave({user: this.user})
        }
        this.cancel = function(){
            this.onCancel({user: this.user})
        }
    }
})